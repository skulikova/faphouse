# faphouse



## Installation

Install all necessary node modules and playwright browsers using the following commands:
```
yarn install
npx playwright install
```

## Add Structure files

| Folder   | Description                                                                    |
|----------|--------------------------------------------------------------------------------|
| config   | Playwright config with test settings                                           |
| e2e      | Autotests                                                                      |
| fixtures | Aggregates of pages                                                            |
| pages    | Contains a detailed description of each feature                                |
| utils    | The smallest and simplest parts of the autotest structure |


## Launch all tests in all projects
```
npx playwright test -c _e2e/config/playwright.config.ts

```
## Launch  tests via Google Chrome
```
yarn playwright-chrome
```

## Launch  tests via Safari
```
yarn playwright-safari
```

## Launch  tests via Firefox
```
yarn playwright-firefox
```