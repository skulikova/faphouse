import {test} from '@playwright/test';
import {defaultUrl} from '@faphouse-e2e/utils';
import {AgeConfirmationPage} from '@faphouse-e2e/age-confirmation-page';


interface IPlaywright {
    app: {
        goToApp: (subUrl?: string) => Promise<void>;
        closeTab: (tab: number) => Promise<void>;
        press: (key: string, options?: {delay?: number; qty?: number}) => Promise<void>;
        ageConfirm: () => Promise<void>;
    };
    ageConfirmation: AgeConfirmationPage;
}

export const playwright = test.extend<IPlaywright>({
    app: async ({page, context, ageConfirmation}, use) => {
        const goToApp = async (subUrl?: string) => {
            const nextUrl = subUrl
                ? `${defaultUrl}${subUrl}`
                : `${defaultUrl}`;

            await page.goto(nextUrl);
        };

        const closeTab = async (tab: number) => {
            const allPages = context.pages();
            const tabToClose = allPages[tab - 1];
            await tabToClose?.close();
        };

        const press = async (keyName: string, options?: {delay?: number; qty?: number}) => {
            for (let i = 0; i < (options?.qty ?? 1); i++) {
                await page.keyboard.press(keyName);
                await page.waitForTimeout(options?.delay ?? 300);
            }
        };

        const ageConfirm = async () => {
            await playwright.step('confirm age', async () => {
                await ageConfirmation.agreementContent.ageConfirmationButton.toBeVisible();
                await ageConfirmation.agreementContent.ageConfirmationButton.click();
            });
        };

        await use({
            goToApp,
            closeTab,
            press,
            ageConfirm,
        });
    },

    ageConfirmation: async ({page}, use) => {
        const ageConfirmation = AgeConfirmationPage.init({page});
        await use(ageConfirmation);
    },
});
