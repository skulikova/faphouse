import {playwright as test} from '@faphouse-e2e/control-fixture';
import {AgeConfirmationPage} from '@faphouse-e2e/age-confirmation-page';
import {HeaderPage} from "@faphouse-e2e/app-components-page";
import {SearchVideoPage} from "@faphouse-e2e/search-video-page";

interface SearchCategoryFixture {
    ageConfirmation: AgeConfirmationPage;

    header: HeaderPage;

    searchVideo: SearchVideoPage;
}

export const playwright = test.extend<SearchCategoryFixture>({
    ageConfirmation: async ({page}, use) => {
        const ageConfirmation = AgeConfirmationPage.init({page});
        await use(ageConfirmation);
    },

    header: async ({page}, use) => {
        const header = HeaderPage.init({page});
        await use(header);
    },

    searchVideo: async ({page}, use) => {
        const searchVideo = SearchVideoPage.init({page});
        await use(searchVideo);
    },
});
