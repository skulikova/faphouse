import {ComponentPage, IComponentPageProps} from '@faphouse-e2e/control';

export class VideoCardPage extends ComponentPage {
    public avatar: ComponentPage;

    public videoTitle: ComponentPage;

    public videoLink: ComponentPage;

    constructor(props: IComponentPageProps) {
        super(props);

        this.avatar = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="thumb__title-avatar"]'),
        });

        this.videoTitle = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="thumb__title-video"]'),
        });

        this.videoLink = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="thumb__video-link"]'),
        });
    }

    public async getVideoLink(nth: number): Promise<string> {
        return await this.page.locator(`[data-el-video-position="${nth}"]`)
            .locator('[class="thumb__video-link"]')
            .getAttribute('href');
    }

    public async getVideoTitle(nth: number): Promise<string> {
        return await this.page.locator(`[data-el-video-position="${nth}"]`)
            .locator('[class="thumb__title-video"]').textContent();
    }
}