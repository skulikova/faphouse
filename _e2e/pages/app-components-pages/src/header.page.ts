import {ComponentPage, IComponentPageProps, InputPage} from '@faphouse-e2e/control';

export class HeaderPage extends ComponentPage {
    public searchInput: InputPage;

    public clearSearch: ComponentPage;

    constructor(props: IComponentPageProps) {
        super(props);

        this.searchInput = new InputPage({
            page: this.page,
            locator: this.page.locator('[data-el="NavbarSearch"]')
                .locator('input').nth(0),
        });

        this.clearSearch = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="icon icon_close text-input__clear icon_size_medium"]'),
        });
    }

    public static init(props: Omit<IComponentPageProps, 'locator'>) {
        return new HeaderPage({
            ...props,
            locator: props.page.locator('data-el="GNavbar"'),
        });
    }
}
