import {test as playwright, Locator, Page} from '@playwright/test';

export interface IComponentPageProps {
    page: Page;
    locator: Locator;
}

export class ComponentPage {
    protected page: Page;

    protected locator: Locator;

    constructor(props: IComponentPageProps) {
        this.page = props.page;
        this.locator = props.locator;
    }

    public async click(options?: Parameters<Locator['click']>[0]) {
        await playwright.expect(this.locator).toBeVisible(options);
        await this.locator.hover(options);
        await this.locator.click(options);
    }

    public async dblclick(options?: Parameters<Locator['click']>[0]) {
        await playwright.expect(this.locator).toBeVisible({timeout: 10000});
        await this.locator.dblclick(options);
    }

    public async hover(options?: Parameters<Locator['hover']>[0]) {
        await playwright.expect(this.locator).toBeVisible(options);
        await this.locator.hover(options);
    }

    public count() {
        return this.locator.count();
    }

    public nth(i: number) {
        return this.locator.nth(i);
    }

    public async toBeVisible(timeout?: number) {
        await playwright.expect(this.locator).toBeVisible({timeout: timeout ?? 5000});
    }

    public async toBeHidden(timeout?: number) {
        await playwright.expect(this.locator).toBeHidden({timeout: timeout ?? 5000});
    }

    public async toHaveAttribute(name: string, expected: string | RegExp, options?: {timeout?: number}) {
        await playwright.expect(this.locator).toHaveAttribute(name, expected, options ?? {timeout: 5000});
    }

    public async notToHaveAttribute(name: string, options?: {timeout?: number}) {
        const attribute = await this.locator.getAttribute(name, options ?? {timeout: 5000});

        await playwright.expect(attribute).toBeFalsy();
    }

    public async toHaveText(expected: string | RegExp | (string | RegExp)[], options?: {timeout?: number; useInnerText?: boolean}) {
        const mergedOptions = {
            ...{timeout: 5000},
            ...options,
        };
        await playwright.expect(this.locator).toHaveText(expected, mergedOptions);
    }

    public async notToHaveText(expected: string | RegExp | (string | RegExp)[], options?: {timeout?: number; useInnerText?: boolean}) {
        const mergedOptions = {
            ...{timeout: 5000},
            ...options,
        };
        await playwright.expect(this.locator).not.toHaveText(expected, mergedOptions);
    }

    public async toHaveCount(expected: number, options?: {timeout?: number}) {
        await playwright.expect(this.locator).toHaveCount(expected, options ?? {timeout: 5000});
    }

    public async toHaveValue(expected: string, options?: {timeout?: number}) {
        await playwright.expect(this.locator).toHaveValue(expected, options ?? {timeout: 5000});
    }

    public async toHaveCss(name: string, expected: string, options?: {timeout?: number}) {
        await playwright.expect(this.locator).toHaveCSS(name, expected, options ?? {timeout: 5000});
    }

    public async toContainText(expected: string | RegExp | (string | RegExp)[], options?: {timeout?: number; useInnerText?: boolean}) {
        await playwright.expect(this.locator).toBeVisible({timeout: 5000});
        await playwright.expect(this.locator).toContainText(expected, options);
    }

    public async notToContainText(expected: string | RegExp | (string | RegExp)[], options?: {timeout?: number; useInnerText?: boolean}) {
        await playwright.expect(this.locator).toBeVisible({timeout: 5000});
        await playwright.expect(this.locator).not.toContainText(expected, options);
    }

    public getAttribute(attribute: string) {
        return this.locator.getAttribute(attribute);
    }

    public isVisible() {
        return this.locator.isVisible();
    }

    public async scrollIntoViewIfNeeded() {
        await this.locator.scrollIntoViewIfNeeded();
    }
}
