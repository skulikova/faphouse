import {ComponentPage} from './component.page';

export class InputPage extends ComponentPage {
    public async fill(text: string) {
        await this.locator.fill(text);
    }

    public async clear() {
        await this.locator.fill('');
    }

    public getValue() {
        return this.locator.inputValue();
    }
}
