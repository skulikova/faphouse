import {ComponentPage, IComponentPageProps} from '@faphouse-e2e/control';

export class AgreementContentPage extends ComponentPage {
    public warning: ComponentPage;

    public getero: ComponentPage;

    public gay: ComponentPage;

    public shemale: ComponentPage;

    public disclaimer: ComponentPage;

    public ageConfirmationButton: ComponentPage;

    constructor(props: IComponentPageProps) {
        super(props);


        this.warning = new ComponentPage({
            page: this.page,
            locator: this.locator.locator('[class="content-disclaimer__warning"]'),
        });

        this.getero = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="control_orientation"]')
                .locator('[data-value="straight"]'),
        });

        this.gay = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="control_orientation"]')
                .locator('[data-value="gay"]'),
        });

        this.shemale = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="control_orientation"]')
                .locator('[data-value="shemale"]'),
        });

        this.disclaimer = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="content-disclaimer__disclaimer"]'),
        });

        this.ageConfirmationButton = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="content-disclaimer__controls"]')
                .locator('[id="yes-im-over-18"]'),
        });
    }
}
