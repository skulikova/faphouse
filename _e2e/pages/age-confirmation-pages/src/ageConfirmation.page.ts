import {ComponentPage, IComponentPageProps} from '@faphouse-e2e/control';
import {AgreementContentPage} from './form/agreementContent.page';

export class AgeConfirmationPage extends ComponentPage {
    public header: ComponentPage;

    public agreementContent: AgreementContentPage;

    constructor(props: IComponentPageProps) {
        super(props);


        this.header = new ComponentPage({
            page: this.page,
            locator: this.page.locator('[class="header"]'),
        });

        this.agreementContent = new AgreementContentPage({
            page: this.page,
            locator: this.page.locator('[class="content-disclaimer__inner"]'),
        });
    }

    public static init(props: Omit<IComponentPageProps, 'locator'>) {
        return new AgeConfirmationPage({
            ...props,
            locator: props.page.locator('[data-el="VisitorAgreement"]'),
        });
    }
}
