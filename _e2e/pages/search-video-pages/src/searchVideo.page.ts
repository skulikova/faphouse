import {ComponentPage, IComponentPageProps} from '@faphouse-e2e/control';
import {VideoCardPage} from "@faphouse-e2e/app-components-page";

export class SearchVideoPage extends ComponentPage {
    public title: ComponentPage;

    public resultQuantityText: ComponentPage;

    public resultVideoList: ComponentPage;

    public videoCard: VideoCardPage;

    constructor(props: IComponentPageProps) {
        super(props);

        this.title = new ComponentPage({
            page: this.page,
            locator: this.locator.locator('[class="title"]'),
        });

        this.resultQuantityText = new ComponentPage({
            page: this.page,
            locator: this.locator.locator('[class="paging-details"]'),
        });

        this.resultVideoList = new ComponentPage({
            page: this.page,
            locator: this.locator.locator('[data-el-type="search-list"]'),
        });

        this.videoCard = new VideoCardPage({
            page: this.page,
            locator: this.locator.locator('[data-el="Thumb"]'),
        });
    }

    public async countVideoCards(): Promise<number> {
        const thumbAttribute = 'Thumb';

        const count = await this.page.evaluate((thumbAttr: string) => {
            const elements = document.querySelectorAll(`[data-el="${thumbAttr}"]`);
            return elements.length;
        }, thumbAttribute);

        return count;
    }

    public async getVideo(nth: number) {
        return this.page.locator('[data-el="Thumb"]').nth(nth);
    }
    public static init(props: Omit<IComponentPageProps, 'locator'>) {
        return new SearchVideoPage({
            ...props,
            locator: props.page.locator('[data-el="PageContentWrapper"]'),
        });
    }
}
