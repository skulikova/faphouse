import {playwright} from '@faphouse-e2e/search-category-fixture';

playwright.describe('searchCategory', () => {
    playwright.beforeEach(async ({app}) => {
        await app.goToApp();

        await app.ageConfirm();
    });

    playwright('check unique quantity video in first page as search results [T001]', async ({
        app,
        header,
        searchVideo
    }) => {
        let videoCardCount: number;

        let videoLinkArray: string[] = [];

        let link: string;

        let title: string;

        await playwright.step('type text in search field', async () => {
            await header.searchInput.toBeVisible();
            await header.searchInput.click();

            await header.searchInput.fill('Homemade');

            await app.press('Enter');
        });

        await playwright.step('check if related page is found', async () => {
            await searchVideo.toBeVisible();

            await searchVideo.title.toHaveText('homemade');
        });

        await playwright.step('check quantity of found video', async () => {
            await searchVideo.resultQuantityText.toContainText('Porn video search results: 1-60 of');

             videoCardCount = await searchVideo.countVideoCards();

            playwright.expect(videoCardCount).toEqual(60);
        });

        await playwright.step('check if all videos are unique', async () => {
            for (let i = videoCardCount - 1; i > 0; i--) {

                link = await searchVideo.videoCard.getVideoLink(i);

                title = await searchVideo.videoCard.getVideoTitle(i);

                if (videoLinkArray.includes(link)) {
                    throw new Error(`founded duplicate item with ${title} and link: ${link}`);
                } else {
                    videoLinkArray.push(link);
                }
            }
        });
    });
});
