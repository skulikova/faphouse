import {PlaywrightTestConfig, devices} from '@playwright/test';

const config: PlaywrightTestConfig = {
    testDir: '../e2e',
    testMatch: '**/*.e2e.ts',
    testIgnore: ['**/_e2e/e2e-tests/performance/**', '**/_e2e/e2e-tests/smoke/**'],
    timeout: 120000,
    reportSlowTests: null,
    retries: 0,
    workers: 8,
    globalSetup: './global-setup',
    expect: {
        timeout: 5000,
    },
    reporter: [['line'], ['html']],
    projects: [
        {
            name: 'Google Chrome',
            use: {
                ...devices['Desktop Chrome'],
                headless: false,
                ignoreHTTPSErrors: true,
                video: 'off',
                screenshot: 'on',
                trace: 'on',
                viewport: {width: 1280, height: 720},
            },
        },
        {
            name: 'Firefox',
            use: {
                ...devices['Desktop Firefox'],
                headless: true,
                ignoreHTTPSErrors: true,
                video: 'off',
                screenshot: 'on',
                trace: 'on',
                viewport: {width: 1920, height: 1268},
            },
        },
        {
            name: 'Safari',
            use: {
                ...devices['Desktop Safari'],
                headless: true,
                ignoreHTTPSErrors: true,
                video: 'off',
                screenshot: 'on',
                trace: 'on',
                viewport: {width: 1920, height: 1268},
            },
        },
    ],
};

export default config;
