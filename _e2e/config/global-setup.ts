import {chromium} from '@playwright/test';
import {defaultUrl} from "@faphouse-e2e/utils";

export default async function globalSetup() {
    const browser = await chromium.launch();

    const context = await browser.newContext();

    await context.addCookies([
        {
            name: 'isVisitorAgreementAccepted',
            value: '1',
            domain: defaultUrl,
            path:'/'
        },
    ]);

    const page = await context.newPage();

    await page.goto(defaultUrl);

    await page.waitForLoadState('load');

    await browser.close();
}
