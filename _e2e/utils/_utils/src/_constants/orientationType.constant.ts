export enum OrientationType {
    Straight = 'straight',
    Gay = 'gay',
    Transgender = 'transgender',
}
